﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonsBehaviour : MonoBehaviour
{
    public GameObject StartButton;
    public GameObject InstructionsButton;
    public GameObject ExitButton;
    int level;
    void Start()
    {
        
    }

    void Update()
    {

        if (EventSystem.current.currentSelectedGameObject == ExitButton)
        {
            Application.Quit();
        }
        if (EventSystem.current.currentSelectedGameObject == StartButton)
        {
            //level = PlayerPrefs.GetInt("CurrentLevel");
            SceneManager.LoadScene(4);
        }
        if (EventSystem.current.currentSelectedGameObject == InstructionsButton)
        {
            SceneManager.LoadScene(2);
        }
    }
}
