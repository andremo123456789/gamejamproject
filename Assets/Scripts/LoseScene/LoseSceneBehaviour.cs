﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class LoseSceneBehaviour : MonoBehaviour
{
    public GameObject buttonExit;
    public Text pointsText;
    void Start()
    {
        pointsText.text = Mathf.Abs((PlayerPrefs.GetInt("CurrentLevel") - 1) * 30f).ToString();
        PlayerPrefs.SetInt("CurrentLevel", 1);
    }

    void Update()
    {
        if (EventSystem.current.currentSelectedGameObject == buttonExit)
        {
            SceneManager.LoadScene(0);
        }
    }

}
