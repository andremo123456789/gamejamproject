﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KnifeBehaviour : MonoBehaviour
{
    public bool direction;
    float timer;
    void Start()
    {
        timer = 0f;
        if(direction)
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
    }

    void Update()
    {
        timer += Time.deltaTime;
        if(!direction)
        {
            this.transform.position = new Vector3(this.transform.position.x + 3.5f * Time.deltaTime,this.transform.position.y,this.transform.position.z);
        }
        else
        {
            this.transform.position = new Vector3(this.transform.position.x - 3.5f * Time.deltaTime,this.transform.position.y,this.transform.position.z);
        }
        if(timer >= 10f)
        {
            this.gameObject.SetActive(false);
        }
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == "Clown")
        {
            SceneManager.LoadScene(1);
        }
    }
}
