﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeThrowerBehaviour : MonoBehaviour
{
    float spawnTimer;
    public GameObject KnifeThrower1;
    public GameObject KnifeThrower2;
    public GameObject Knife;

    void Start()
    {
        if(PlayerPrefs.GetInt("CurrentLevel") <= 1)
        {
            this.gameObject.SetActive(false);
        }
    }
    void Update()
    {
        spawnTimer += Time.deltaTime;
        if(PlayerPrefs.GetInt("CurrentLevel") >= 7)
        {
            if(spawnTimer >= (0.75f + (6f / (PlayerPrefs.GetInt("CurrentLevel") - 5))))
            {
                if(Random.Range(0f, 10f) >= 5f)
                {
                    GameObject knife = Instantiate(Knife, KnifeThrower1.transform);
                    knife.GetComponent<KnifeBehaviour>().direction = false;
                    spawnTimer = 0f;
                }
                else
                {
                    GameObject knife = Instantiate(Knife, KnifeThrower2.transform);
                    knife.GetComponent<KnifeBehaviour>().direction = true;
                    spawnTimer = 0f;
                }
            }
        }
        else
        {
            if(spawnTimer >= 8f)
            {
                if(Random.Range(0f, 10f) >= 5f)
                {
                    GameObject knife = Instantiate(Knife, KnifeThrower1.transform);
                    knife.GetComponent<KnifeBehaviour>().direction = false;
                    spawnTimer = 0f;
                }
                else
                {
                    GameObject knife = Instantiate(Knife, KnifeThrower2.transform);
                    knife.GetComponent<KnifeBehaviour>().direction = true;
                    spawnTimer = 0f;
                }
            }
        }
    }
}
