﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class InstructionsSceneBehaviour : MonoBehaviour
{
    public GameObject exitButton;
    void Start()
    {
        
    }

    void Update()
    {
        if (EventSystem.current.currentSelectedGameObject == exitButton)
        {
            SceneManager.LoadScene(0);
        }
    }
}
