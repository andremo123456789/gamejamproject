﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimerBehaviour : MonoBehaviour
{
    float timer;
    void Start()
    {
        timer = 30f;
    }

    void Update()
    {
        timer -= Time.deltaTime;
        this.gameObject.GetComponent<Text>().text = ((int)timer).ToString();
        if(timer <= 0f)
        {
            PlayerPrefs.SetInt("CurrentLevel", PlayerPrefs.GetInt("CurrentLevel") + 1);
            timer = 30f;
            SceneManager.LoadScene(4);
        }
    }
}
