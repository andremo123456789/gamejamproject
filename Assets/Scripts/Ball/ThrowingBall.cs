﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ThrowingBall : MonoBehaviour
{
    public KeyCode hotkey;
    public GameObject textHint;
    float timeCounter;
    bool started;
    public GameObject pivotTransform;
    public float levelActivation;
    public GameObject glass;
    public Animator clownAnimation;
    public AudioSource audioSource;
    void Start()
    {
        if(PlayerPrefs.GetInt("CurrentLevel") < levelActivation)
        {
            if(glass != null)
            {
                glass.SetActive(false);
            }
            this.gameObject.SetActive(false);
        }
        textHint.GetComponent<MeshRenderer>().sortingLayerName = "Foreground";
        textHint.GetComponent<MeshRenderer>().sortingOrder = 50;
    }

    void Update()
    {
        transform.RotateAround(pivotTransform.transform.position, Vector3.back, 0.75f);
        timeCounter += Time.deltaTime;

        if(Input.GetKey(hotkey) && started)
        {
            if(!clownAnimation.GetBool("Crounching"))
            {
                audioSource.Play();
                textHint.GetComponent<TextMesh>().color = Color.green;
                started = false;
            }
        }
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "BallStartTrigger")
        {
            started = true;
            textHint.GetComponent<TextMesh>().color = Color.red;
        }
        if(collider.gameObject.tag == "BallEndTrigger")
        {
            if(started == true && timeCounter >= 0.2f)
            {
                SceneManager.LoadScene(1);
            }
            else
            {
                timeCounter = 0f;
            }
        }
    }
}
