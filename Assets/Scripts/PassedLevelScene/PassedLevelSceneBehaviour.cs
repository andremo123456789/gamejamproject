﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PassedLevelSceneBehaviour : MonoBehaviour
{
    public GameObject balance;
    public GameObject knifes;
    public GameObject balls;
    public GameObject nextButton;
    void Start()
    {
        if(PlayerPrefs.GetInt("CurrentLevel") == 1)
        {
            balance.SetActive(true);
        }
        else if(PlayerPrefs.GetInt("CurrentLevel") == 2)
        {
            knifes.SetActive(true);
            this.gameObject.GetComponent<AudioSource>().Play();
        }
        else
        {
            this.gameObject.GetComponent<AudioSource>().Play();
            balls.SetActive(true);
        }
    }
    void Update()
    {
        Debug.Log(PlayerPrefs.GetInt("CurrentLevel"));
        if (EventSystem.current.currentSelectedGameObject == nextButton)
        {
            SceneManager.LoadScene(3);
        }
        
    }
}
