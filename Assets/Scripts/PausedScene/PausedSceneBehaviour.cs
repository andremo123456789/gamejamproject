﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class PausedSceneBehaviour : MonoBehaviour
{
    public GameObject exitButton;
    public GameObject continueButton;
    void Start()
    {
    }

    void Update()
    {
        if (EventSystem.current.currentSelectedGameObject == exitButton)
        {
            SceneManager.LoadScene(0);
        }
        if (EventSystem.current.currentSelectedGameObject == continueButton)
        {
            SceneManager.LoadScene(3);
        }
    }
}
