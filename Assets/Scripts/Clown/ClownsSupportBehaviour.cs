﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClownsSupportBehaviour : MonoBehaviour
{
    Rigidbody2D rigidbody;
    Transform transform;
    float RandomTimer;
    public Animator clownAnimator;
    public GameObject ball1;
    public GameObject ball2;
    public GameObject ball3;
    public GameObject ball4;
    public CircleCollider2D clownCollider;
    //public AudioSource crounchAudio;
    void Start()
    {
        rigidbody = this.GetComponent<Rigidbody2D>();
        transform = this.GetComponent<Transform>();
        RandomTimer = 1f;
        if(PlayerPrefs.GetInt("CurrentLevel") >= 3)
        {
            clownAnimator.SetBool("Throwing", true);
        }
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.DownArrow))
        {
            // if(!clownAnimator.GetBool("Crounching"))
            // {
            //     crounchAudio.Play();
            // }
            clownAnimator.SetBool("Crounching", true);
            clownCollider.enabled = false;
        }
        else
        {
            clownAnimator.SetBool("Crounching", false);
            clownCollider.enabled = true;
        }
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            //transform.eulerAngles = new Vector3(0,0,transform.eulerAngles.z + 40f * Time.deltaTime);
            rigidbody.MoveRotation(rigidbody.rotation + 10f * Time.deltaTime);
        }
        if(Input.GetKey(KeyCode.RightArrow))
        {
            rigidbody.MoveRotation(rigidbody.rotation - 10f * Time.deltaTime);
            //transform.eulerAngles = new Vector3(0,0,transform.eulerAngles.z - 40f * Time.deltaTime);
        }
        //transform.position = new Vector3(transform.position.x, 0f, transform.position.z);
        if(transform.position.x >= 0.5f)
        {
            transform.position = new Vector3(0.5f, transform.position.y, transform.position.z);
        }
        if(transform.position.x <= -0.5f)
        {
            transform.position = new Vector3(-0.5f, transform.position.y, transform.position.z);
        }
    }
}
